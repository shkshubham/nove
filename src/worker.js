const cacheName = 'pwa-app-cache';
const filesToCache = [
  './',
  './index.html',
  './script.js'
];

// This triggers when user starts the app
self.addEventListener('install', function(e) {
  e.waitUntil(
    caches.open(cacheName).then(function(cache) {
      return cache.addAll(filesToCache);
    })
  );
});

// Delete old caches
self.addEventListener('activate',  event => {

  event.waitUntil(self.clients.claim());
});


// Here we intercept request and serve up the matching files
self.addEventListener('fetch', event => {
   
    if(event.request.url.endsWith('style.css')){
        event.respondWith(
            fetch('./style.css')
       );
    }

    //check redit urls
    if(event.request.url.endsWith("https//www.reddit.com")){
        console.log(event.request.url, "worker")
        event.respondWith(
            fetch(event.request).then(res =>{
              if(res.ok){
                return res;
              }
              return new Response({
                data: {
                  dist: 0,
                  message: "Currently App is offline"
                }
              });
            }).catch(error =>{
              return new Response({ data: {
                  dist: 0,
                  message: "Currently App is offline"
                }
              })
            })
        );
    }
});