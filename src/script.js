const container = document.getElementById("search-section");
const contentSection = document.getElementById("content-section");
const pageInfoSection = document.getElementById("page-info-section");
const searchData = document.getElementById('search');
const subredditData = document.getElementById('sub-reddit');

addEventListenerToInput(searchData)
addEventListenerToInput(subredditData)

function addEventListenerToInput(input_field){
    input_field.addEventListener("keyup", function(event) {
        event.preventDefault();
        if (event.keyCode === 13) {
            fetchDataRedditData();
        }
    });
}

(function() {
    setContentSectionHeight()
})();
const counterSection = document.getElementById("counter");

let state = {
    apiData: [],
    start: 0
}

async function fetchDataRedditData(){
    const fromDate = getDate(document.getElementById("from-date").value);
    const toDate = getDate(document.getElementById("to-date").value);
    const postFrom = generateReddit(getDateDiff(fromDate, toDate));
    setState({
        apiData: await fetchData(checkAndGenerateSubReddit(subredditData.value), searchData.value, postFrom)
    });
    setState({
        start: 0
    });
    renderRedditPosts(false, 10);
}

async function fetchData(subReddit, searchQuery, post_from){
    contentSection.innerHTML = renderSpinner();
    try {
       const { data } = await axios.get(generateURL(subReddit,searchQuery, post_from));
       return data.data;
    } catch(err){
        renderNoPostFound();
        return {
            dist: 0,
            children: []
        };
    }
}

function generateURL(subReddit, searchQuery, post_from){
    const host = `https://www.reddit.com`
    let url = host;
    let backUrl = `&sort=top&restrict_sr=on&t=${post_from}`;
    if(searchQuery.length){
        url = `${host}/search.json?q=${searchQuery}${backUrl}`
    } else if(subReddit.length){
        url = `${host}/r/${subReddit}.json`
    }
    if(searchQuery && subReddit){
        url = `${host}/r/${subReddit}/search.json?q=${searchQuery}${backUrl}`
    }
    console.log(url)
    return url;
}

function renderRedditPosts(type, offset){
    const {
        dist
    } = state.apiData
    if(!dist){
        return renderNoPostFound();
    }
    contentSection.innerHTML = type ? contentSection.innerHTML + generateRedditPosts(offset) : "" + generateRedditPosts(offset)
}

function generateRedditPosts(offset){
    let body = '';
    const {
        children,
        dist
    } = state.apiData;
    if(offset >= dist){
        offset = dist
    }
    const {
        start
    } = state;
    for(let index = start; index < offset; index++){
        const {
            title,
            preview
        } = children[index].data
        if(preview){
            const {
                url
            } = preview.images[0].source
            body = body + generatePostBody(title,url)
        } else {
            body = body + generatePostBody(title,"")
        }
    }
    setState({
        start: offset
    })

    pageInfoSection.innerHTML = renderPageInfoSection();

    return body;
}

function generatePostBody(title, thumbnail){
    return `
        <div class="col">
            <img class="post-img" src ="${thumbnail.includes("external-preview") 
            ? thumbnail 
            : "https://getuikit.com/v2/docs/images/placeholder_200x100.svg"}" />
            <div>${title}</div>
        </div>
    `
}

function renderNoPostFound(){
    contentSection.innerHTML =  "<div><h4>No Post found</h4></div>";
}

function renderSpinner(){
    return `<div class="spinner"></div>`
}

contentSection.addEventListener("scroll", () => {
    var contentSectionScrollTop = contentSection.scrollTop;
    var contentSectionWindowHeight = contentSection.scrollHeight;
    var bodyHeight = contentSectionWindowHeight - contentSection.offsetHeight;
    var scrollPercentage = (contentSectionScrollTop / bodyHeight);

    // if the scroll is more than 90% from the top, load more content.
    if(scrollPercentage > 0.9) {
        if(state.apiData.dist > state.start){
            const {
                start,
                apiData
            } = state;
            if(start <= apiData.dist){
                renderRedditPosts(true, start + 10)
            }
        }
    }
})

function setContentSectionHeight(){
    const {
        offsetHeight
    } = container;
    const {
        innerHeight
    } = window;
    contentSection.style.maxHeight = `${innerHeight - offsetHeight}px`;
}

function renderPageInfoSection(){

}

function setState(value){
    state = {
        ...state,
        ...value,
    }
}


function getDateDiff(start_date, to_date){
    return parseInt((to_date - start_date) / (1000 * 60 * 60 * 24)); 
}

function getDate(date){
    if(date.length){
        return new Date(date)
    }
    return "";
}

function generateReddit(date_diff){
    const days = [1, 7, 30];
    console.log(date_diff)
    switch (true)
    {
        case (date_diff <= days[0]):
            return "day"
        case (date_diff < days[1] && date_diff >= days[0]):
            return "week"
        case (date_diff < days[2] && date_diff >= days[1]):
            return "month"
        default:
            return "all"
    }
}

function checkAndGenerateSubReddit(subreddit){
    const isMultiple = subreddit.replace(/ +/g, "").split(',');
    return isMultiple.length > 1 ? isMultiple.join("+") : subreddit;
}


function renderPageInfoSection(){
    const {
        start,
        apiData
    } = state;
    return `
        <div>
            Showing ${start} of ${apiData.dist}
        </div>   
        <hr />
    ` 
}